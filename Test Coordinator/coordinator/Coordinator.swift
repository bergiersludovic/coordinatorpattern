//
//  Coordinator.swift
//  Test Coordinator
//
//  Created by Bergiers Ludovic on 15/07/2019.
//  Copyright © 2019 Bergiers Ludovic. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator : AnyObject{
    
    var childCoordinators : [Coordinator] {get set}
    var navigationController : UINavigationController {get set}
    
    func start()
}
