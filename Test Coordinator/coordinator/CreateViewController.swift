//
//  ViewController.swift
//  Test Coordinator
//
//  Created by Bergiers Ludovic on 15/07/2019.
//  Copyright © 2019 Bergiers Ludovic. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController , Storyboarded{

    weak var coordinator : MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    @IBAction func buyTapped(_ sender: UIButton) {
        coordinator?.buySubscription()
    }
    
    @IBAction func backMenuTapped(_ sender: UIButton) {
        coordinator?.start()
        
    }
}

