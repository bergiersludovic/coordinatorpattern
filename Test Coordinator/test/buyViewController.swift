//
//  buyViewController.swift
//  Test Coordinator
//
//  Created by Bergiers Ludovic on 15/07/2019.
//  Copyright © 2019 Bergiers Ludovic. All rights reserved.
//

import UIKit

class buyViewController: UIViewController, Storyboarded{

    weak var coordinator : MainCoordinator?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backToCreateTapped(_ sender: UIButton) {
        coordinator?.createAccount()
    }
    
   
}
