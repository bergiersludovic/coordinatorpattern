//
//  DebutViewController.swift
//  Test Coordinator
//
//  Created by Bergiers Ludovic on 15/07/2019.
//  Copyright © 2019 Bergiers Ludovic. All rights reserved.
//

import UIKit

class DebutViewController: UIViewController, Storyboarded {

    weak var coordinator : MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


    
    @IBAction func CreateTapped(_ sender: UIButton) {
        coordinator?.createAccount()
    }
    
}
