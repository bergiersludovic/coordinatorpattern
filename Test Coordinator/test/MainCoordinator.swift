//
//  MainCoordinator.swift
//  Test Coordinator
//
//  Created by Bergiers Ludovic on 15/07/2019.
//  Copyright © 2019 Bergiers Ludovic. All rights reserved.
//

import UIKit

class MainCoordinator : NSObject, Coordinator, UINavigationControllerDelegate {
    
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    init(navigationController : UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = DebutViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func buySubscription() {
        let vc = buyViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func createAccount(){
        let vc = CreateViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    
}
